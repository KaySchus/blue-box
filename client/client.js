// Constants and useful stuff
var defaultWidth = 8;
var defaultHeight = 8;
var defaultPixelSize = 64;

// Instances used globally in this file
var currentAnimation;
var currentTimeout;

// Handlebars init, occurs before init() is called
var source = $("#animation_list").html();
var template = Handlebars.compile(source);

loadJSON("../server/animations.json", function(data) {
	$(".animation-list .content").html(template(data.animations));
});

function init() {
	renderer = PIXI.autoDetectRenderer(
		defaultWidth * defaultPixelSize,
		defaultHeight * defaultPixelSize,
		{ view:document.getElementById("game-canvas") }
	);
}

function loadAnimation(url, name) {
	stage = new PIXI.Container();
	graphics = new PIXI.Graphics();

	animation = new Animation();

	loadJSON(url, function(data) {
		console.log(url);
		console.log(name);
		var targetAnimation = getValue(data, name);

		animation.width = targetAnimation.width;
		animation.height = targetAnimation.height;
		animation.frames = targetAnimation.frames;
		animation.sequence = targetAnimation.sequence;
		animation.loop = targetAnimation.loop;

		animation.colors = targetAnimation.colors;

		console.log(targetAnimation.colors);

		animation.borderColor = targetAnimation.borderColor;
		animation.borderWidth = targetAnimation.border;
		animation.borderCurve = targetAnimation.borderCurve;
		animation.pixelSize = targetAnimation.pixelSize;

		animation.init();

		if (currentAnimation == undefined || currentAnimation.finished) {
			renderer.resize(animation.width * animation.pixelSize, animation.height * animation.pixelSize);
			currentAnimation = animation;
			update();
		} else {
			currentAnimation.completed = function() {
				clearTimeout(currentTimeout);
				renderer.resize(animation.width * animation.pixelSize, animation.height * animation.pixelSize);
				currentAnimation = animation;
				update();
			}
		}
	});
}

function update() {
	graphics.clear();
	graphics.lineStyle(currentAnimation.borderWidth, currentAnimation.borderColor);

	for (y = 0; y < currentAnimation.height; y++) {
		for (x = 0; x < currentAnimation.width; x++) {
			graphics.beginFill(currentAnimation.colors[currentAnimation.currentFrame.map[currentAnimation.height * y + x]]);
			var ps = currentAnimation.pixelSize;
			graphics.drawRoundedRect(x * ps, y * ps, ps, ps, currentAnimation.borderCurve);
		}
	}

	currentAnimation.update();

	stage.addChild(graphics);
	renderer.render(stage);

	if (currentAnimation.finished != true) {
		currentTimeout = setTimeout(update, currentAnimation.currentFrame.time);
	}

	if (currentAnimation.completed != undefined) {
		currentAnimation.completed();
	}
}

function Animation() {
	this.frames = [];
	this.sequence = [];
	this.width = 0;
	this.height = 0;

	this.colors = [];
	this.borderColor = 0;
	this.borderWidth = 0;
	this.borderCurve = 0;
	this.pixelSize = 32;

	this.currentFrameCounter = 0;
	this.currentFrame = null;

	this.loop = false;

	this.finished = false;

	this.init = function() {
		this.currentFrameCounter = 0;
		this.currentFrame = this.frames[this.sequence[this.currentFrameCounter]];
	}

	this.update = function() {
		this.currentFrameCounter = this.currentFrameCounter + 1;
		this.finished = false;

		if (this.currentFrameCounter === this.sequence.length) {
			if (this.loop == true) {
				this.currentFrameCounter = 0;
			} else {
				this.finished = true;
				return;
			}
		}

		this.currentFrame = this.frames[this.sequence[this.currentFrameCounter]];
	}
}