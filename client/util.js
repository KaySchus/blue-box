function loadJSON(url, callback) {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET", url, true);
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
			var jsonData = JSON.parse(xmlhttp.responseText);
			console.log(jsonData.animations);
			callback(jsonData);
		}
	}

	xmlhttp.send();
}

function getValue(json, targetKey) {
	if (targetKey in json.animations) {
		return json.animations[targetKey];
	} else {
		return null;
	}
}

Handlebars.registerHelper('link', function(url, name) {
  	var url = Handlebars.escapeExpression(url);
    var text = Handlebars.escapeExpression(name);

  	
    var string = "javascript:loadAnimation('" + url + "','" + text + "');";
    console.log(string);
    return string;
});